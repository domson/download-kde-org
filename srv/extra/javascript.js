var mbRoot = document.getElementById("mirrorbrain-details");

function removeElements() {
	// Remove "powered by" message
	var mbAddr = mbRoot.getElementsByTagName("address")[0];
	mbRoot.removeChild(mbAddr);
}

function makeDownloadBox() {
	var header = mbRoot.getElementsByTagName("h2")[0];
	var download = header.textContent.replace("Mirrors for ", "");
	var url = header.textContent.split("/");
	var file = url[url.length - 1];

	// Create a notification box
	var box = document.createElement("div");
	box.setAttribute("class", "alert alert-info");

	// Box title, filename and button
	var title = document.createElement("h3");
	title.innerHTML = "<i class=\"icon-info-sign\"></i> File information";

	var filename = document.createElement("li");
	filename.innerHTML = "Filename: <b>" + file + "</b>";

	var button = document.createElement("a");
	button.setAttribute("href", download);
	button.setAttribute("class", "btn btn-primary");
	button.innerHTML = "<i class=\"icon-download-alt icon-white\"></i> Download file";

	var line = document.createElement("hr");

	mbRoot.replaceChild(box, header);
	box.appendChild(title);

	// Move the file information
	var info = mbRoot.getElementsByTagName("ul")[0];
	info.insertBefore(filename, info.firstChild);

	mbRoot.removeChild(info);
	box.appendChild(info);
	box.appendChild(line);
	box.appendChild(button);
}

function fixupInfoLinks() {
	var blockquotes = mbRoot.getElementsByTagName("ul");

	if (blockquotes.length > 0) {
		var blockquote = blockquotes[0];
		var links = blockquote.getElementsByTagName("li");

		for (var i = 0; i < links.length; ++i) {
				if (links[i].textContent.indexOf("BitTorrent") != -1 ) {
					blockquote.removeChild(links[i]);
				}
		}
	}
}

function fixupMetalinks() {
	// Create the new heading for metalinks
	var newHeader = document.createElement("h3");
	newHeader.appendChild(document.createTextNode("Metalinks:"));

	var blockquotes = mbRoot.getElementsByTagName("blockquote");

	if (blockquotes.length > 0) {
		var blockquote = blockquotes[0];

		// Iterate over all links in the blockquote and create an <ul> with the
		// links as list items.
		var links = blockquote.getElementsByTagName("a");
		var linkList = document.createElement("ul");

		for (var i = 0; i < links.length; ++i) {
			if (links[i].text.indexOf("torrent") == -1 &&
				links[i].text.indexOf("magnet") == -1) {
				var listItem = document.createElement("li");
				listItem.appendChild(links[i].cloneNode(true));
				linkList.appendChild(listItem);
			}
		}

		// The blockquote is preceded by a <br>, which we'll delete,
		// otherwise it will cause too much white space
		// once we add the <h3> (which has its own top-margin in the CSS)
		var br = blockquote.previousElementSibling;

		if (br.nodeName == 'BR') {
			br.parentNode.removeChild(br);
		}

		// Replace the blockquote with our new elements.
		// The new elements have to be added before deleting the blockquote,
		// since we use the blockquote as the reference element
		// to know where to add the new stuff.
		blockquote.parentNode.insertBefore(newHeader, blockquote);
		blockquote.parentNode.insertBefore(linkList, blockquote);
		blockquote.parentNode.removeChild(blockquote);
	}
}

function findInfoElem() {
	var parlist = mbRoot.getElementsByTagName("p");

	for (var i = 0; i < parlist.length; i++) {
		if (parlist[i].textContent.indexOf("List of best mirrors for IP address") == 0) {
			return parlist[i];
		}
	}

	return null;
}

function moveInfoAway() {
	var infoElem = findInfoElem();

	if (infoElem.parentNode != mbRoot) {
		console.log("info elem not where we expected... won't continue");
		return false;
	}

	var regex = /\):[ \n\r\xa0]*$/;

	for (var e = infoElem.lastChild; e; e = e.previousSibling) {
		if (e.nodeType == document.TEXT_NODE && regex.test(e.data)) {
			e.data = e.data.replace(regex, ').');
			break;
		}
	}

	mbRoot.removeChild(infoElem);
	mbRoot.appendChild(infoElem);

	return true;
}

function makeGreyBoxes() {
	var headings = mbRoot.getElementsByTagName("h3");
	var lists = mbRoot.getElementsByTagName("ul");
	var boxes = new Array();

	// Copy all lists to a box
	for (var i = 1; i < headings.length; i++) {
		var box = document.createElement("div");
		box.setAttribute("class", "grey-box");

		var heading = document.createElement("h3");
		heading.innerHTML = "<i class=\"icon-globe\"></i> " + headings[i].innerHTML.replace(":", "");

		var list = document.createElement("ul");
		list.innerHTML = lists[i].innerHTML;

		box.appendChild(heading);
		box.appendChild(list);

		boxes[i - 1] = box;
	}

	// Remove the existing lists
	for (var i = 1; i <= (lists.length - 1); i++ ) {
		mbRoot.removeChild(headings[1]);
		mbRoot.removeChild(lists[1]);

		mbRoot.appendChild(boxes[i - 1]);
	}

	// Do the same for the <p> block
	var paragraph = mbRoot.getElementsByTagName("p")[0];
	paragraph.innerHTML = paragraph.innerHTML.replace(":", ".");

	var box = document.createElement("div");

    mbRoot.removeChild(paragraph);
    mbRoot.appendChild(box);

    box.setAttribute("class", "alert alert-success");
	box.appendChild(paragraph);
}

function fixupMirrorList() {
	if ($("table").length > 0) {
		$("table").addClass("table");
		$("table img").addClass("thumbnail");
		$("#pageRow br").remove();

		// Hide in-between headers
		$("tr").each(function() {
			var firstCol = $(this).children().first();

			if (firstCol.attr("colspan") == 6 && !firstCol.hasClass("newregion")) {
				$(this).remove();
			}
		});

		// Fix table stripe
		flag = false;

		$("tbody tr").each(function() {
			if ($(this).children().first().hasClass("newregion")) {
				flag = true;
			} else {
				if (flag) {
					$(this).addClass("odd");
				} else {
					$(this).removeClass("odd");
				}

				flag = !flag;
			}
		});

		// Add a link to the mirrors map
		$("table").after(
			'<center><i class="icon-globe"></i> ' +
			'<a href="/extra/download-mirrors-map.html">Map of all KDE Mirrors</a>' +
			'</center>'
		);
		// Add a link to the files mirrors map
		$("table").after(
			'<center><i class="icon-globe"></i> ' +
			'<a href="/extra/files-mirrors-map.html">Map of all KDE Application Data Mirrors</a>' +
			'</center>'
		);
	}
}

if (window.location.href.indexOf("/extra/") != -1 ) {
	fixupMirrorList();
} else {
	removeElements();
	makeDownloadBox();
	fixupInfoLinks();
	fixupMetalinks();
	moveInfoAway();
	makeGreyBoxes();
}
