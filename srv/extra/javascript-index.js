// Original height of the message box
var originalHeight;

// Execute upon document ready
$(function() {
	// Wrap everything in a container
	$("body").html(
		'<div class="container">' +
			$("body").html() +
		'</div>'
	);
	
	// Generate a page header
	$("body").prepend(
		'<div class="navbar navbar-static-top Neverland">' +
			'<div class="navbar-inner">' +
				'<div class="container">' +
					'<div class="nav-collapse">' +
						'<ul class="nav pull-right">' +
							'<li>' +
								'<a href="//download.kde.org/extra/download-mirrors.html">' +
									'<i class="icon-download"></i> ' +
									'Official KDE Mirrors' +
								'</a>' +
							'</li>' +
							'<li>' +
								'<a href="//files.kde.org/extra/files-mirrors.html">' +
									'<i class="icon-list"></i> ' +
									'KDE Application Data Mirrors' +
								'</a>' +
							'</li>' +
						'</ul>' +
					'</div>' +
					'<a href="//kde.org" class="brand">' +
						'<img src="//cdn.kde.org/img/logo.plain.small.png" id="site_logo" alt=""> KDE' +
					'</a>' +
				'</div>' +
			'</div>' +
		'</div>'
	);

	// Prettify tables
	$("table").addClass("table table-striped");

	// Generate a page footer
	$("address").html(
		'<p>Powered by <a href="http://mirrorbrain.org/">MirrorBrain</a> - Maintained by ' +
		'<a href="//bugs.kde.org/enter_sysadmin_request.cgi">KDE Sysadmin</a> - ' +
		'<a href="//www.kde.org/mirrors/ftp_howto.php">Want to be a mirror?</a><br />' +
		'KDE<sup>&reg;</sup> and <a href="//www.kde.org/media/images/trademark_kde_gear_black_logo.png"> ' +
		'the K Desktop Environment<sup>&reg;</sup> logo</a> are registered trademarks of ' +
		'<a href="//ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | ' +
		'<a href="//www.kde.org/community/whatiskde/impressum.php">Legal</a></p>'
	);

	// Do these only if the folder has a message text
	if ($("pre").length > 0) {
		// Replace the code with a alert box
		var lines = $("pre").text().trim("\n").split("\n");

		if (lines.length > 1) {
			$("pre").replaceWith(
				'<div class="alert alert-info">' +
					'<div class="message-toggle pull-right">' +
						'<i class="icon-chevron-down"></i>' +
					'</div>' +
					'<div class="message-title">' +
						'<i class="icon-info-sign"></i> Information about this folder' +
					'</div>' +
					'<pre class="message">' +
						$("pre").html().trim("\n") +
					'</pre>' +
					'<div class="message-metadata hide"></div>' +
				'</div>'
			);
		} else {
			$("pre").replaceWith(
				'<div class="alert alert-info">' +
					'<pre><i class="icon-info-sign"></i> ' + $("pre").html().trim("\n") + '</pre>' +
				'</div>'
			);

			$(".alert-info").css("minHeight", 20);
		}

		// Save the original height
		originalHeight = $(".message").innerHeight();

		// Collapse on startup
		$(".message-title").show();
		$(".message").css({
			opacity: 0,
			height: 0,
			display: "none"
		});

		// Toggle container visibility
		$(".message-toggle").click(function() {
			if ($(".message").is(":visible")) {
				$(".message").animate({
					height: 0,
					opacity: 0
				}, function() {
					$(this).hide();
				});


				$(".message-title").fadeIn();
				$(".message-toggle i")
					.removeClass("icon-chevron-up")
					.addClass("icon-chevron-down");
			} else {
				$(".message").show();
				$(".message").animate({
					height: originalHeight,
					opacity: 1
				});
				$(".message-title").fadeOut();
				$(".message-toggle i")
					.removeClass("icon-chevron-down")
					.addClass("icon-chevron-up");
			}
		});
	}

	// Add icons to files/folders
	$("tbody tr").each(function() {
		if ($(this).children().last().text().trim().length == 0) {
			$(this).children().eq(1).prepend(
				'<i class="icon-folder-close"></i> '
			);
		} else if ($(this).children().last().text() != "Metadata" ) {
			$(this).children().eq(1).prepend(
				'<i class="icon-file"></i> '
			);
		}

		if ($(this).children().eq(1).text().trim().toLowerCase() == "parent directory") {
			var html = $(this).children().eq(1).html().replace("icon-folder-close", "icon-arrow-up");
			$(this).children().eq(1).html(html);
		}
	});
});
