#!/bin/bash 

QUERY='\t \\ \pset format Unaligned \\ select lat,lng,identifier,operator_name,operator_url from server;'
echo $QUERY | psql -o /srv/mirrorbrain/download.kde.org.txt -h localhost -U download download
echo -e "lat\tlon\ttitle\tdescription\ticon\ticonSize\ticonOffset" > /srv/mirrorbrain/download.kde.org-1.txt 
cat /srv/mirrorbrain/download.kde.org.txt | awk -F'|' '{print $1"\t"$2"\t"$4"\t"$3"<br>Website: <a href=\""$5"\" target=\"_new\">"$5"</a><br>Lat: "$1"<br>Long: "$2"\t//www.kde.org/media/images/kde.png\t16,16\t-8,-8"}' >> /srv/mirrorbrain/download.kde.org-1.txt
echo >> /srv/mirrorbrain/download-1.kde.org.txt
mv /srv/mirrorbrain/download.kde.org-1.txt /srv/mirrorbrain/extra/download-mirrors-map-data.txt
rm /srv/mirrorbrain/download.kde.org.txt

QUERY='\t \\ \pset format Unaligned \\ select lat,lng,identifier,operator_name,operator_url from server;'
echo $QUERY | psql -o /srv/mirrorbrain/files.kde.org.txt -h localhost -U files files
echo -e "lat\tlon\ttitle\tdescription\ticon\ticonSize\ticonOffset" > /srv/mirrorbrain/files.kde.org-1.txt 
cat /srv/mirrorbrain/files.kde.org.txt | awk -F'|' '{print $1"\t"$2"\t"$4"\t"$3"<br>Website: <a href=\""$5"\" target=\"_new\">"$5"</a><br>Lat: "$1"<br>Long: "$2"\t//www.kde.org/media/images/kde.png\t16,16\t-8,-8"}' >> /srv/mirrorbrain/files.kde.org-1.txt
echo >> /srv/mirrorbrain/files-1.kde.org.txt
mv /srv/mirrorbrain/files.kde.org-1.txt /srv/mirrorbrain/extra/files-mirrors-map-data.txt
rm /srv/mirrorbrain/files.kde.org.txt
