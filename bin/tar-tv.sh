#!/bin/bash

function find.query () {
  set -o noglob
  set -f
  [ "$1" ] \
  && printf -- $'-iname\n%s\n-o\n' "${@#/}" | head -n -1 \
  || true
}

function find.archives () {
  e=($(printf -- $'*.%s\n' "${!EXT[@]}"))
  q=($(find.query "${e[@]}"))
  find $ROOT -type f \( "${q[@]}" \)
}

function gen.tar-tv () {
  a=${1:?$FUNCNAME:No archive path given}
  rel="$(realpath -s --relative-to "$ROOT" "$a")"
  l="$rel.$TARGET_EXT"
  p="$(dirname "$TARGET/$l")"
  
  $NOBASE \
  && l="$(basename "$l")"

  for check_ext in $(echo ${!EXT[@]} | sort -r); do
    if [[ "$(basename "$a")" = *$check_ext* ]]; then
      echo ${EXT[$check_ext]} -tv -f "$(realpath $ROOT)/$rel" ">$(realpath "$TARGET")/$l" >&2
      [ ! -d "$p" ] && mkdir -p "$p"
      ${EXT[$check_ext]} -tv -f "$ROOT/$rel" >"$TARGET/$l"
      break
    fi
  done
}


declare ROOT=. TARGET=. TARGET_EXT=tar-tv NOBASE=false
declare -a ARCHIVES=()
declare -A EXT=([tar]="tar" \
                [tar.gz]="tar -z" \
                [tar.bz2]="tar -j" \
                [tar.xz]="tar -J")

while [ $# -gt 0 ]; do
    case "$1" in
        -r|--root)
            [[ ! "$2" = -* ]] \
            && { ROOT="$2"; shift; } \
            || { echo "Err: No root path given. quit." && exit 1; }
            ;;
        -t|--target)
            [[ ! "$2" = -* ]] \
            && { TARGET="$2"; shift; } \
            || { echo "Err: No target directory given. quit." && exit 1; }
            ;;
        -e|--extension)
            [[ ! "$2" = -* ]] \
            && { TARGET_EXT="$2"; shift; } \
            || { echo "Err: No target extension given. quit." && exit 1; }
            ;;
        -n|--nobase)
            NOBASE=true
            ;;
        -h|--help)
            cat <<HELP
Usage: tar-tv [OPTION]... [ARCHIVE]...
Generate archive content lists from ARCHIVE

With no ARCHIVE, find archives in root directory or in current working directory.

    --help|-h                - print this help
    --root|-r <directory>    - directory to generate tar lists from
    --target|-t <directory>  - alternative target direcotry (default:[ROOT|cwd])
    --extension|-e           - custom extension (default:tar-tv)
    --nobase|-n              - generate lists flat into target directory
HELP
            exit
            ;;
        -*)
            echo "Err: Unkown option $1. quit." && exit
            ;;
        *)
            ARCHIVES+=("$1");
            ;;
    esac
    shift
done

[ ! -d "$ROOT" ] \
&& echo "Err: Root '$ROOT' is no directory. quit" && exit 1

[ ! -d "$TARGET" ] && { mkdir -p "$TARGET" || exit 1; }

[ ! -d "$TARGET" ] \
&& echo "Err: Target '$TARGET' is no directory. quit" && exit 1


while read -r archive; do
  cd "$ROOT"
  gen.tar-tv "$archive" \
  || echo "Warn: Could not generate archive content list for '$archive'."
  cd - >/dev/null
done < <(
  if [ ${#ARCHIVES[@]} -eq 0 ]; then
    find.archives
  else
    find "${ARCHIVES[@]}" -type f
  fi
)
